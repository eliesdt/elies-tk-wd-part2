var API_AUTHOR_URL = "https://reststop.randomhouse.com/resources/authors/"
var API_BOOK_URL = "https://reststop.randomhouse.com/resources/works/"
var CORS_ANYWHERE = "http://cors-anywhere.herokuapp.com/"

var authors = ["140632","2140314","185896"];
var AUTHOR_ID = authors[Math.floor(Math.random() * authors.length)];

sampleAuthorData = {
    authordisplay: "Eric Ries",
    spotlight: "<b>Eric Ries&#160;</b>is an entrepreneur&#160;and author of the popular blog&#160;Startup Lessons Learned. He cofounded and served as CTO of IMVU, his third startup,&#160;and has had plenty of startup failures along the way.&#160;He is a frequent speaker at business events, has advised a number of startups, large companies, and venture capital firms on business and product strategy, and is an entrepreneur-in-residence at Harvard Business School. His Lean Startup methodology has been written about in the&#160;<i>New York Times</i>, the&#160;<i>Wall Street Journal</i>, the&#160;<i>Harvard Business Review</i>, the&#160;<i>Huffington Post</i>, and many blogs. He lives in San Francisco.",
    works: { works:  ['210088','250646','420174'] }
}

sampleBookData = {
    "titleAuth": "The Lean Startup : Eric Ries",
    "titleSubtitleAuth": "The Lean Startup : How Todays Entrepreneurs Use Continuous Innovation to Create Radically Successful Businesses : Eric Ries"
}

function populateBookInformation(jsonData) {
    console.log(jsonData)

    var titleSplit = jsonData.titleSubtitleAuth.split(" : ")
    var title = titleSplit[0]
    var subtitle = titleSplit[1]
    if (subtitle) {
        bookInfo = title + ": " + subtitle;
    } else bookInfo = title;

    var element = document.createElement("li");
    var titleText = document.createTextNode(bookInfo);
    element.appendChild(titleText);
    document.getElementById("author-books-list").appendChild(element);
    document.getElementById("author-books").style.display = "block";
}

function getBookInformation(BOOK_ID) {
    var BOOK_URL = CORS_ANYWHERE + API_BOOK_URL + BOOK_ID
    fetch(BOOK_URL, {
        method: 'GET',
        headers: {      
                    "Accept": "application/json",
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Origin": "*"
                }
    })
    .then((response) => {
        return response.json();
    })
    .then((jsonData) => {
        populateBookInformation(jsonData)
        console.log(jsonData);
    })
    .catch((e) => {
        console.log("Error: "+e+" going to use demo data");
        populateBookInformation(sampleBookData);
    });
}

function populateAuthorInformation(jsonData) {
    console.log(jsonData)
    var name = document.getElementById("author-name");
    name.innerHTML = jsonData.authordisplay;
    var description = document.getElementById("author-description");
    description.innerHTML = jsonData.spotlight;

    var booksRawArray = jsonData.works.works;
    let booksArray = [...new Set(booksRawArray)];
    console.log(booksArray);
    for (index = 0; index < booksArray.length; index++) { 
        console.log(booksArray[index]);
        getBookInformation(booksArray[index]);
    }
}

function getAuthorInformation(AUTHOR_ID) {
    var AUTHOR_URL = CORS_ANYWHERE + API_AUTHOR_URL + AUTHOR_ID
    fetch(AUTHOR_URL, {
        method: 'GET',
        headers: {      
                    "Accept": "application/json",
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Origin": "*"
                }
    })
    .then((response) => {
        return response.json();
    })
    .then((jsonData) => {
        console.log(jsonData);
        populateAuthorInformation(jsonData);
    })
    .catch((e) => {
        console.log("Error: "+e+" going to use demo data");
        populateAuthorInformation(sampleAuthorData);
    });
}

document.addEventListener("DOMContentLoaded", getAuthorInformation(AUTHOR_ID), false);

//document.addEventListener("DOMContentLoaded", getAuthorInformation("140632"), false)
//document.addEventListener("DOMContentLoaded", populateAuthorInformation(sampleAuthorData), false)
//document.addEventListener("DOMContentLoaded", getBookInformation("210088"), false)
//document.addEventListener("DOMContentLoaded", populateBookInformation(sampleBookData), false)

//authorInfo = getAuthorInformation()
//console.log(authorInfo)